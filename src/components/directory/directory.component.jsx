import React from 'react'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'

import { selectDirectorySections} from '../../redux/directory/directory.selectors'
import MenuItem from '../menu-item/menu-item.component'

import './directory.styles.scss'

// we need to store state value, thus we use class instead of function
const Directory  = ({ sections }) => (
  <div className='directory-menu'>
    {sections.map(({id, ...otherSectionsProps}) => (
        <MenuItem
            key={id}
            { ...otherSectionsProps }
        />
      ))}
  </div>
)

const mapStateToProps = createStructuredSelector({
  sections: selectDirectorySections
})

export default connect(mapStateToProps)(Directory);