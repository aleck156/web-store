import React from 'react'

import { CustomButtonContainer } from './custom-button.styles'

const CustomButton = ({ children, ...props }) => (
    // when this element gets type='submit' passed, it'll automatically be include in button props
    <CustomButtonContainer {...props}>
        { children }
    </CustomButtonContainer>
)

export default CustomButton;