import React, { useState } from 'react'
import { connect } from 'react-redux'

import FormInput from '../form-input/form-input.component'
import CustomButton from '../custom-button/custom-button.component'

import { signUpStart } from '../../redux/user/user.actions'

import './sign-up.styles.scss'

const SignUp = ({signUpStart}) => {
  const [userCredentials, setUserCredentials] = useState({
    displayName:      '',
    email:            '',
    password:         '',
    confirmPassword:  ''
  });

  const { displayName, email, password, confirmPassword } = userCredentials;

  const handleSubmit = async (event) => {
    // we don't want to perform default action, yet
    event.preventDefault();
    if (password !== confirmPassword) {
      alert("Passwords don't match, at all!")
      return;
    }

    signUpStart({ email, password, displayName })
  }

  const handleChange = event => {
    const { name, value } = event.target;
    setUserCredentials({
      ...userCredentials,
      [name]: value
    })
  }

  return (
    <div className='sign-up'>
      <h2 className='title'>I do not have an account yet</h2>
      <span>Sign up with your email and password</span>
      <form className='sign-up-form' onSubmit={ handleSubmit }>
        <FormInput
          type='text'
          name='displayName'
          value={ displayName }
          onChange={ handleChange }
          label='Display Name'
          autoComplete='username'
          required
        />
        <FormInput
          type='email'
          name='email'
          value={ email }
          onChange={ handleChange }
          label='Email'
          autoComplete='email'
          required
        />
        <FormInput
          type='password'
          name='password'
          value={ password }
          onChange={ handleChange }
          label='Password'
          autoComplete='new-password'
          required
        />
        <FormInput
          type='password'
          name='confirmPassword'
          value={ confirmPassword }
          onChange={ handleChange }
          label='Confirm Password'
          autoComplete='new-password'
          required
        />
        <CustomButton type='submit'>Sign Up, bro!</CustomButton>
      </form>
    </div>
  )
}

const mapDispatchToProps = dispatch => ({
  signUpStart:    (userCredentials) => dispatch(signUpStart(userCredentials))
})

export default connect(null, mapDispatchToProps)(SignUp);