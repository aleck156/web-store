import React from 'react'
import StripeCheckout from 'react-stripe-checkout'

const onToken = token => {
  console.log(token);
  alert('Payment Successful, SNAFU')
}

const StripeCheckoutButton = ({ price }) => {
  const priceForStripe = price * 100;
  const publishableKey = 'pk_test_51Hey4sJumeFsxMd88UY3F9HhtEI3OGKx7DW3wfBDYMilN9eVcvYT14YuI0gTLrZS9dvwbd01dPOzHJKVUHM23XKu00BB9TiTZ7';
  const topLabel = 'Pay Now';

  return (
    <StripeCheckout
      label={topLabel}
      name='eShop Ltd.'
      billingAddress
      shippingAddress
      image='https://sendeyo.com/up/d/f3eb2117da'
      description={`Your total is $ ${price}`}
      amount={priceForStripe}
      panelLabel={topLabel}
      token={onToken}
      stripeKey={publishableKey}
    />
  )
}

export default StripeCheckoutButton;