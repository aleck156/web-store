import UserActionTypes from './user.types'

/*
  state - original data that will be affected
  action - what action will be performed on the abovementioned state
  INITIAL_STATE - default data that's created when new object was built for the first time
*/
const INITIAL_STATE = {
  currentUser:  null,
  error:        null,
}

const userReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case UserActionTypes.SIGN_IN_SUCCESS:
      return {
        ...state,
        currentUser:  action.payload,
        errror:       null,
      }

    case UserActionTypes.SIGN_OUT_SUCCESS:
      return {
        ...state,
        currentUser:  null,
        error:        null,
      }
    case UserActionTypes.SIGN_IN_FAILURE:
    case UserActionTypes.SIGN_OUT_FAILURE:
    case UserActionTypes.SIGN_UP_FAILURE:
      return {
        ...state,
        error:  action.payload
      }

    // if no action is performed, return the initial data provided to this function
    default:
      return state;
  }
}

export default userReducer;