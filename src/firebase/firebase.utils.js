import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'

const config = {
  apiKey: "AIzaSyDELL66mJ62zgg9HIufUqWaKmhAz6GGKt0",
  authDomain: "ecom-db-dd235.firebaseapp.com",
  databaseURL: "https://ecom-db-dd235.firebaseio.com",
  projectId: "ecom-db-dd235",
  storageBucket: "ecom-db-dd235.appspot.com",
  messagingSenderId: "554165787774",
  appId: "1:554165787774:web:e19ce1bcbccd440256cc2f",
  measurementId: "G-TPHXQ2KWV4"
}

// async, becasue we're making web api call
export const createUserProfileDocument = async (userAuth, additionalData) => {
  if (!userAuth) return;
  const userRef = firestore.doc(`users/${userAuth.uid}`);
  // const collectionRef = firestore.collection('users');

  const snapShot = await userRef.get();
  
  // if the snapshot doesn't exist, create new one
  if (!snapShot.exists){
    const {displayName, email} = userAuth;
    const createdAt = new Date();
    // async request to the database, thus try/catch block is necessary
    try {
      await userRef.set({
        displayName,
        email,
        createdAt,
        ...additionalData
      })
    } catch (error) {
      console.log('Error creating user: ', error.message);
    }
  }

  return userRef;
}

export const addCollectionAndDocuments = async (collectionKey, objectsToAdd) =>{
  const collectionRef = firestore.collection(collectionKey);
  
  const batch = firestore.batch();
  objectsToAdd.forEach(obj => {
    const newDocRef = collectionRef.doc();
    batch.set(newDocRef, obj);
  });
  return await batch.commit();
}

// convert into an object an array we get from firebase
export const convertCollectionsSnapshotToMap = (collections) => {
  const transformedCollection = collections.docs.map(doc => {
    const { title, items} = doc.data();

    return {
      routeName:  encodeURI(title.toLowerCase()),
      id:         doc.id,
      title,
      items
    }
  })

  return transformedCollection.reduce((accumulator, collection) => {
    accumulator[collection.title.toLowerCase()] = collection;
    return accumulator;
  }, {})
}

// initialize database connection
firebase.initializeApp(config);

export const getCurrentUser = () => {
  return new Promise((resolve, reject) => {
    const unsubscribe = auth.onAuthStateChanged(userAuth => {
      unsubscribe();
      resolve(userAuth);
    }, reject)
  })
}

export const auth = firebase.auth();
export const firestore = firebase.firestore();

/*
    wheneve user clicks 'Log In using google account'
    this will pop up google accounts window
*/
export const googleProvider = new firebase.auth.GoogleAuthProvider();
googleProvider.setCustomParameters({ prompt: 'select_account'})
export const signInWithGoogle = () => auth.signInWithPopup(googleProvider);

export default firebase;