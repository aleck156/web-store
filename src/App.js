import React, { useEffect } from 'react';
import './App.css';
import { Switch, Route, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'

// project imports
import HomePage from './pages/homepage/homepage.component.jsx'
import ShopPage from './pages/shop/shop.component.jsx'
import Header   from './components/header/header.component.jsx'
import SignInAndSignUpPage from './pages/sign-in-and-sign-up/sign-in-and-sign-up.component.jsx'
import CheckOutPage from './pages/checkout/checkout.component'

import { selectCurrentUser } from './redux/user/user.selectors'
import { checkUserSession } from './redux/user/user.actions'

const App = ({ checkUserSession, currentUser }) => {
  useEffect(() => {
    checkUserSession();
  }, [checkUserSession]);

  return (
    <div>
    <Header />
    <Switch>
      <Route exact path='/' component={ HomePage } />
      <Route exact path='/checkout' component={ CheckOutPage } />
      <Route path='/shop' component={ ShopPage } />
      <Route exact 
        path='/signIn'
        render={() => currentUser ?
          <Redirect to='/' />
          :
          <SignInAndSignUpPage />
        }
      />
    </Switch>
  </div>
  );
}

const mapStateToProps = createStructuredSelector({
  currentUser:        selectCurrentUser,
})

const mapDispatchToProps = dispatch => ({
  checkUserSession:   () => dispatch(checkUserSession())
})

// first argument is set to null, as App doesn't need to store any user information
// redux will handle storing any currentUser data 
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);