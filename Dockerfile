FROM node:latest

# Create app directory
RUN mkdir -p /usr/src/app

# move into that directory
WORKDIR /usr/src/app

# download project repo

# copy package.json, with all pacakges necessary to run this app
COPY package.json /usr/src/app

# install packages into container
RUN npm install

# connect app files stored locally with folders inside container
ADD src /usr/src/app/src
ADD public /usr/src/app/public

# build the app
RUN npm run build

# run the app
CMD ["npm", "start"]

# build your own image
# docker build --tag e-com:3.0 .

# run your own container
# docker container run -d -p 8000:5000 --name ecom e-com:3.0

# kill it with fire!
# docker rm --force ecom

# get inside, bro!
# docker exec -it ecom bash